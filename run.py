import flask
import psycopg2
import logging
import pprint
import datetime


class Db:
    _conn = 0
    _cursor = 0

    def __init__(self, dbname):
        self._conn = psycopg2.connect(database=dbname)
        self._cursor = self._conn.cursor()

    def query(self, query_string):
        self._cursor.execute(query_string)
        return self._cursor.fetchall()

    def first_result(self, query_string):
        return self.query(query_string)[0][0]

    def return_name(self, role, id):
        if role not in ['manager', 'operator', 'developer']:
            raise
        return self.first_result('select name from {}_{}s where id = {}'.format(prefix,role,id))


def who_is_oncall_today():
    return db.query("""select
             {0}_managers.name as manager,
             {0}_operators.name as operator,
             {0}_developers.name as developer
             from {0}_coordination
             inner join {0}_managers on manager_id = {0}_managers.id
             inner join {0}_operators on operator_id = {0}_operators.id
             inner join {0}_developers on developer_id = {0}_developers.id
             where date = '{1}'
             """.format(prefix, datetime.date.today().isoformat()))[0]


db = Db('jharasym')
prefix = 'oncall'


if __name__ == '__main__':
    pprint.pprint(who_is_oncall_today())
