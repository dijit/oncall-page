--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.1
-- Dumped by pg_dump version 9.6.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;
SET search_path = public, pg_catalog;
SET default_tablespace = '';
SET default_with_oids = false;

--
-- Name: oncall_coordination; Type: TABLE; Schema: public; Owner: jharasym
--

CREATE TABLE oncall_coordination (
    date date NOT NULL,
    operator_id integer,
    manager_id integer,
    developer_id integer
);


ALTER TABLE oncall_coordination OWNER TO jharasym;

--
-- Name: oncall_developers; Type: TABLE; Schema: public; Owner: jharasym
--

CREATE TABLE oncall_developers (
    id integer NOT NULL,
    name character varying(40) NOT NULL,
    mobile character varying(14),
    CONSTRAINT oncall_developers_mobile_check CHECK (((mobile)::text ~ '^[0-9]+$'::text))
);


ALTER TABLE oncall_developers OWNER TO jharasym;

--
-- Name: oncall_developers_id_seq; Type: SEQUENCE; Schema: public; Owner: jharasym
--

CREATE SEQUENCE oncall_developers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE oncall_developers_id_seq OWNER TO jharasym;

--
-- Name: oncall_developers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jharasym
--

ALTER SEQUENCE oncall_developers_id_seq OWNED BY oncall_developers.id;


--
-- Name: oncall_managers; Type: TABLE; Schema: public; Owner: jharasym
--

CREATE TABLE oncall_managers (
    id integer NOT NULL,
    name character varying(40) NOT NULL,
    mobile character varying(14),
    CONSTRAINT oncall_managers_mobile_check CHECK (((mobile)::text ~ '^[0-9]+$'::text))
);


ALTER TABLE oncall_managers OWNER TO jharasym;

--
-- Name: oncall_managers_id_seq; Type: SEQUENCE; Schema: public; Owner: jharasym
--

CREATE SEQUENCE oncall_managers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE oncall_managers_id_seq OWNER TO jharasym;

--
-- Name: oncall_managers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jharasym
--

ALTER SEQUENCE oncall_managers_id_seq OWNED BY oncall_managers.id;


--
-- Name: oncall_operators; Type: TABLE; Schema: public; Owner: jharasym
--

CREATE TABLE oncall_operators (
    id integer NOT NULL,
    name character varying(40) NOT NULL,
    mobile character varying(14),
    CONSTRAINT oncall_operators_mobile_check CHECK (((mobile)::text ~ '^[0-9]+$'::text))
);


ALTER TABLE oncall_operators OWNER TO jharasym;

--
-- Name: oncall_operators_id_seq; Type: SEQUENCE; Schema: public; Owner: jharasym
--

CREATE SEQUENCE oncall_operators_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE oncall_operators_id_seq OWNER TO jharasym;

--
-- Name: oncall_operators_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: jharasym
--

ALTER SEQUENCE oncall_operators_id_seq OWNED BY oncall_operators.id;


--
-- Name: oncall_developers id; Type: DEFAULT; Schema: public; Owner: jharasym
--

ALTER TABLE ONLY oncall_developers ALTER COLUMN id SET DEFAULT nextval('oncall_developers_id_seq'::regclass);


--
-- Name: oncall_managers id; Type: DEFAULT; Schema: public; Owner: jharasym
--

ALTER TABLE ONLY oncall_managers ALTER COLUMN id SET DEFAULT nextval('oncall_managers_id_seq'::regclass);


--
-- Name: oncall_operators id; Type: DEFAULT; Schema: public; Owner: jharasym
--

ALTER TABLE ONLY oncall_operators ALTER COLUMN id SET DEFAULT nextval('oncall_operators_id_seq'::regclass);


--
-- Data for Name: oncall_coordination; Type: TABLE DATA; Schema: public; Owner: jharasym
--

COPY oncall_coordination (date, operator_id, manager_id, developer_id) FROM stdin;
2016-12-09	1	1	1
\.


--
-- Data for Name: oncall_developers; Type: TABLE DATA; Schema: public; Owner: jharasym
--

COPY oncall_developers (id, name, mobile) FROM stdin;
1	Bjorn Lindberg	\N
2	Tim Karlsson	\N
3	Mikael Andersson	\N
4	Finn Broman	\N
5	Rasmus Neckleman	\N
6	Ahmad Mousen	\N
\.


--
-- Name: oncall_developers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jharasym
--

SELECT pg_catalog.setval('oncall_developers_id_seq', 6, true);


--
-- Data for Name: oncall_managers; Type: TABLE DATA; Schema: public; Owner: jharasym
--

COPY oncall_managers (id, name, mobile) FROM stdin;
1	Fredrik Bronjemark	\N
2	Kevin Goyon	\N
3	Mircea Niculae	\N
\.


--
-- Name: oncall_managers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jharasym
--

SELECT pg_catalog.setval('oncall_managers_id_seq', 3, true);


--
-- Data for Name: oncall_operators; Type: TABLE DATA; Schema: public; Owner: jharasym
--

COPY oncall_operators (id, name, mobile) FROM stdin;
1	Jan Harasym	0046761669615
2	Dawid Pogorzelski	\N
\.


--
-- Name: oncall_operators_id_seq; Type: SEQUENCE SET; Schema: public; Owner: jharasym
--

SELECT pg_catalog.setval('oncall_operators_id_seq', 2, true);


--
-- Name: oncall_coordination oncall_coordination_pkey; Type: CONSTRAINT; Schema: public; Owner: jharasym
--

ALTER TABLE ONLY oncall_coordination
    ADD CONSTRAINT oncall_coordination_pkey PRIMARY KEY (date);


--
-- Name: oncall_developers oncall_developers_pkey; Type: CONSTRAINT; Schema: public; Owner: jharasym
--

ALTER TABLE ONLY oncall_developers
    ADD CONSTRAINT oncall_developers_pkey PRIMARY KEY (id);


--
-- Name: oncall_managers oncall_managers_pkey; Type: CONSTRAINT; Schema: public; Owner: jharasym
--

ALTER TABLE ONLY oncall_managers
    ADD CONSTRAINT oncall_managers_pkey PRIMARY KEY (id);


--
-- Name: oncall_operators oncall_operators_pkey; Type: CONSTRAINT; Schema: public; Owner: jharasym
--

ALTER TABLE ONLY oncall_operators
    ADD CONSTRAINT oncall_operators_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

